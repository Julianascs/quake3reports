/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import br.com.inlocomedia.quakeiiireports.Match;
import br.com.inlocomedia.quakeiiireports.Quake3ConsoleReport;
import br.com.inlocomedia.quakeiiireports.Quake3WebReport;
import br.com.inlocomedia.quakeiiireports.ReportableGame;
import java.io.File;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author juliana.souza
 */
public class Quake3Tests {

    private ReportableGame consoleReport;
    private ReportableGame webReport;

    @Before
    public void setUp() {
        File file = new File("C:\\Users\\juliana.souza\\Documents\\QuakeReports\\log.txt");
        this.consoleReport = new Quake3ConsoleReport(file);
        this.webReport = new Quake3WebReport(file);
    }

    // Checks if an application detects a file that does not exist
    @Test(expected = RuntimeException.class)
    public void fileDoesNotExistsTest() {
        File file = null;
        ReportableGame report = new Quake3ConsoleReport(file);
    }

    // Check if the matches have been processed.
    @Test
    public void matchesIsNotEmptyTest() {
        Map<String, Match> matches = this.consoleReport.getMatches();
        assertNotNull(matches);
        assertFalse(matches.isEmpty());
    }
    
    // Checks for players with zero death
    @Test
    public void playersWithZeroKillsTest() {
        Map<String, Match> map = this.consoleReport.getMatches();
        Collection<Match> matches = map.values();
        boolean containsZero = false;
        for (Match match : matches) {
            Map<String, Integer> kills = match.getKills();
            if (kills.containsValue(new Integer(0))) {
                containsZero = true;
            }
        }
        assertFalse(containsZero);
    }
    
    // Check if there are matches with the total other than zero, 
    //but with the sum of the dead equal to zero
    @Test
    public void totalDifZeroButThereIsNoKillsTest() {
        Map<String, Match> map = this.consoleReport.getMatches();
        Collection<Match> matches = map.values();
        for (Match match : matches) {
            int total = match.getTotal();
            if (total != 0) {
                Map<String, Integer> killsMap = match.getKills();
                Collection<Integer> kills = killsMap.values();
                int sum = 0;
                for (Integer kill : kills) {
                    sum += kill;
                }
                assertNotEquals(0, sum);
            }
        }
    }
    
    // Verifies that the sum of the "kills" is equal to the total of deaths
    @Test
    public void totalKillEquallPlayersKillsSumTest() {
        Map<String, Match> map = this.consoleReport.getMatches();
        Collection<Match> matches = map.values();
        for (Match match : matches) {
            int total = match.getTotal();
            Map<String, Integer> killsMap = match.getKills();
            Collection<Integer> kills = killsMap.values();
            int sum = 0;
            for (Integer kill : kills) {
                sum += kill;
            }
            assertEquals(total, sum);
        }
    }
    
    
    // Check if the names in "kill" are in "players"
    @Test
    public void namesKilledsContainsInPlayers(){
        Map<String, Match> map = this.consoleReport.getMatches();
        Collection<Match> matches = map.values();
        boolean contains = false;
        for (Match match : matches) {
            Set<String> namePlayers = match.getPlayers();
            Map<String, Integer> kills = match.getKills();
            for (String namePlayer : namePlayers) {
                if(kills.containsKey(namePlayer)){
                    contains = true;
                }
            }
        }
        assertTrue(contains);
    }
}
