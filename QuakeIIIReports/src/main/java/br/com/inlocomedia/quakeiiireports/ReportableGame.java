/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.inlocomedia.quakeiiireports;

import java.util.Map;

/**
 * 
 * 
 * @author juliana.souza
 */
public interface ReportableGame {

    /**
     * Prints the Game Summary as a pretty printed JSON .
     */
    void printGameSummary();
    
    /**
     * Generate the list of players kills.
     */
    void generateGameRanking();
    
    /**
     * Get the matches from the log file
     * 
     * @return 
     */
    Map<String, Match> getMatches();
}
