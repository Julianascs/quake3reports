/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.inlocomedia.quakeiiireports;

import java.io.File;
import java.util.Map;

/**
 * Responsible to generate a Quake3 console report.
 * 
 * @author juliana.souza
 */
public class Quake3ConsoleReport extends AbstractQuake3Report implements ReportableGame {

    public Quake3ConsoleReport(File file) {
        super(file);
    }

    @Override
    public void generateGameRanking() {
        
        System.out.println("******** Game Ranking (Console List) ********");
        
        Map<String, Integer> sortedMap = super.makeGameRanking();
        
        for (Map.Entry<String, Integer> entry : sortedMap.entrySet()) {
            System.out.printf("Player: %s --> Total Kills: %d \n", entry.getKey(), entry.getValue());
        }
        
        System.out.println();
    }
}
