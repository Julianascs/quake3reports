/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.inlocomedia.quakeiiireports;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author juliana.souza
 */
public enum LogValuesENUM {

    //Regular expressions to filter information in the log
    BLANK_LINE("\\d{1,2}:\\d\\d -{1,}"),
    GAME_START("\\d{1,2}:\\d\\d InitGame:"),
    END_OF_THE_GAME("\\d{1,2}:\\d\\d ShutdownGame:"),
    DEATH_ACTION("([<A-z> ]{0,})killed([A-z ]{0,})by");

    private final String regex;

    private LogValuesENUM(String regex) {
        this.regex = regex;
    }

    //Method to check if the line read in the code
    //caters to some of the regular expressions
    public static boolean verifyLineFromLog(LogValuesENUM item, String lineFromLog) {
        Pattern pattern = Pattern.compile(item.regex);
        Matcher matcher = pattern.matcher(lineFromLog);
        return matcher.find();
    }

    public static String returnFilteredLine(LogValuesENUM item, String lineFromLog) {
        Pattern pattern = Pattern.compile(item.regex);
        Matcher matcher = pattern.matcher(lineFromLog);
        if (matcher.find()) {
            lineFromLog = matcher.group();
        }
        return lineFromLog;
    }
}
