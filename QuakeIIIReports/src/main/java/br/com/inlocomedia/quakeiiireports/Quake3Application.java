/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.inlocomedia.quakeiiireports;

import java.io.File;

/**
 * The main class of the project.
 *
 * @author juliana.souza
 */
public class Quake3Application {

    public static void main(String[] args) {

        if (args.length == 0 || args[0].trim().isEmpty()) {
            throw new RuntimeException("You need to specify the path of the log file!");
        } else {
            File file = new File(args[0]);
            ReportableGame report = new Quake3ConsoleReport(file);
            report.printGameSummary();
            report.generateGameRanking();
            report = new Quake3WebReport(file);
            report.generateGameRanking();
        }
    }
}
