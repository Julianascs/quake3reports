/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.inlocomedia.quakeiiireports;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author juliana.souza
 */
public class Match {
    
    private final transient String WORLD_KEY = "<world>";
    private int total;
    private Set<String> players;
    private Map<String, Integer> kills;

    public Match() {
        this.players = new HashSet<>();
        this.kills = new HashMap<>();
    }
    
    public void addKill(String playerName) {
        int count;
        if (this.kills.containsKey(playerName.trim())) {
            count = this.kills.get(playerName);
            count++;
        } else {
            count = 1;
        }
        this.kills.put(playerName, count);
        this.total++;
    }
    
    public void addPlayer(String playerName) {
        if (!WORLD_KEY.equalsIgnoreCase(playerName)) {
            this.players.add(playerName);
        }
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public Set<String> getPlayers() {
        return players;
    }

    public void setPlayers(Set<String> players) {
        this.players = players;
    }

    public Map<String, Integer> getKills() {
        return kills;
    }

    public void setKills(Map<String, Integer> kills) {
        this.kills = kills;
    }
}
