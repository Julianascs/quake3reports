/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.inlocomedia.quakeiiireports;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author juliana.souza
 */
public class Quake3WebReport extends AbstractQuake3Report implements ReportableGame {

    public Quake3WebReport(File file) {
        super(file);
    }

    @Override
    public void generateGameRanking() {

        System.out.println("******** Game Ranking (Web File) ********");

        Map<String, Integer> sortedMap = super.makeGameRanking();

        StringBuilder sb = new StringBuilder();
        sb.append("<html>");
        sb.append("<head>");
        sb.append("<title>Quake III - Ranking");
        sb.append("</title>");
        sb.append("</head>");
        sb.append("<body>");
        sb.append("<h3>Quake III - Web ranking</h3>");

        for (Map.Entry<String, Integer> entry : sortedMap.entrySet()) {
            sb.append(String.format("<p>Player: <b>%s</b>  --> Total kills: <b>%s</b></p>", entry.getKey(), entry.getValue()));
        }

        sb.append("</body>");
        sb.append("</html>");

        try (
                FileWriter fstream = new FileWriter("webRanking.html");
                BufferedWriter out = new BufferedWriter(fstream)) {
            out.write(sb.toString());
            System.out.println("****** webRanking.html file created. Run it in a Browser. *********");
        } catch (IOException ex) {
            System.out.println("There was an error when tryed to create the HTML file:" + ex.getMessage());
        }
    }

}
