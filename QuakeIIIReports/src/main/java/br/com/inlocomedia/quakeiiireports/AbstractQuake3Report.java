/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.inlocomedia.quakeiiireports;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Class responsible to make the mainly file processes.
 * 
 * @author juliana.souza
 */
public abstract class AbstractQuake3Report implements ReportableGame {
    
    private final File file;
    protected Map<String, Match> matches;

    /**
     * Main constructor of the class.
     * 
     * @param file The log file.
     */
    public AbstractQuake3Report(File file) {
        this.file = file;
        this.processGameLog();
    }
    
    private void processGameLog() {
        
        if (!this.isFileOK()) {
            throw new RuntimeException("The file does not exist. Please enter a valid file path.");
        }
        
        this.matches = new TreeMap<>();
        
        List<String> lines = this.readLines();
        
        Match match = null;

        for (String lineFromLog : lines) {
            if (LogValuesENUM.verifyLineFromLog(LogValuesENUM.GAME_START, lineFromLog)) {
                if (match == null) {
                    match = new Match();
                }
            } else if (LogValuesENUM.verifyLineFromLog(LogValuesENUM.DEATH_ACTION, lineFromLog)) {
                String filteredLine = LogValuesENUM.returnFilteredLine(LogValuesENUM.DEATH_ACTION, lineFromLog);
                registerKill(filteredLine, match);
            } else if (LogValuesENUM.verifyLineFromLog(LogValuesENUM.END_OF_THE_GAME, lineFromLog)) {
                int countMatches = this.matches.size() + 1;
                this.matches.put("game_" + countMatches, match);
                match = null;
            }
        }
    }
    
    /**
     * Verify if the file is not null and exists.
     * 
     * @return true if exists, otherwise false.
     */
    private boolean isFileOK() {
        return this.file != null && this.file.exists();
    }

    /**
     * Give the lines from the log file.
     * 
     * @return List of strings representing the file.
     */
    private List<String> readLines() {
        List<String> lines = new ArrayList<>();
        try {
            lines = Files.readAllLines(this.file.toPath());
        } catch (IOException e) {
            System.out.printf("Error opening file: %s.\n",
                    e.getMessage());
        }
        return lines;
    }

    @Override
    public void printGameSummary() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(this.matches);
        System.out.println("");
        System.out.println("******** Game Summary (JSON) ********");
        System.out.println(json);
        System.out.println("");
    }

    /**
     * Create a ascending sorted map of the players ranking.
     * 
     * @return 
     */
    protected Map<String, Integer> makeGameRanking() {

        Map<String, Integer> result = new TreeMap<>();
        
        for (Match currentMatch : this.matches.values()) {
            for (Map.Entry<String, Integer> entry : currentMatch.getKills().entrySet()) {
                String playerName = entry.getKey();
                Integer numberOfKills = entry.getValue();
                if (result.containsKey(playerName)) {
                    int playerTotal = result.get(playerName);
                    result.put(playerName, playerTotal + numberOfKills);
                } else {
                    result.put(playerName, numberOfKills);
                }
            }
        }
        
        return MapUtil.sortByValue(result);
    }
    
    
    /**
     * Set the information about a player kill.
     * 
     * @param record
     * @param match 
     */
    private void registerKill(String record, Match match) {

        String[] recordData = record.split(" killed ");
        String playerKiller = recordData[0].trim();
        String playerKilled = recordData[1].substring(0, recordData[1].length() - 3).trim();
        match.addPlayer(playerKiller);
        match.addPlayer(playerKilled);
        match.addKill(playerKilled);
    }

    @Override
    public Map<String, Match> getMatches() {
        return this.matches;
    }
}
