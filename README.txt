

Project QuakeIIIReports



---Running the project in the Console---


To execute the project via command line, do the following steps:


1st Command: cd "<path_to_the_project_jar>"


2nd Command: java -cp QuakeIIIReports-1.0-jar-with-dependencies.jar br.com.inlocomedia.quakeiiireports.Quake3Application "<path_where_the_log_file_will_be_created>"





---Testing the Project ---

To execute the tests without errors, it is necessary to pass a log file, so that the tests may do the validations correctly. To do that, open the class "Quake3Tests.java" and, inside the method setUp(), set the "file" variable with a valid path containing an existant log file.

Example: File file = new File("C:\\Users\\juliana.souza\\Documents\\QuakeReports\\log.txt");